.. image:: https://readthedocs.org/projects/ml-flow-py-client/badge/?version=latest
    :target: https://ml-flow-py-client.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

|

.. image:: https://cdn.sanity.io/images/9k7dfruw/development/kzMhkj0Mu0Br_V7RheVbquFiOsilkV4Mq5HNy-347x60.svg
    :alt: Analytika
    :target: https://www.analytika.no/


|
|


=================
ML-Flow-Py-Client
=================


The simple insurance model building tool for rapid development and deployment.


Guide for developers.


Quickstart
==========

dont use: python setup.py develop

pip install .



New version
===========

Pre-Commit
==========

pip install pre-commit
pre-commit install


Windows
=======
wslconfig /setdefault Ubuntu

git commit --no-verify

pypi
====

Commit all changes
***********************************
git add .
git commit -m "message"

Tag the version
***********************************
git tag -l vx.x.xx



Create distributions
***********************************
python3 setup.py bdist_wheel



Upload to pypi
***********************************
twine upload -u $PYPI_USERNAME -p PYPI_PASSWORD dist/*



Docs
====
https://pyscaffold.org/en/latest/faq.html



