.. image:: https://readthedocs.org/projects/ml-flow-py-client/badge/?version=latest
    :target: https://ml-flow-py-client.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

|

.. image:: https://cdn.sanity.io/images/9k7dfruw/development/kzMhkj0Mu0Br_V7RheVbquFiOsilkV4Mq5HNy-347x60.svg
    :alt: Analytika
    :target: https://www.analytika.no/


|
|


##############
ML-Flow-Client
##############

The simple insurance model building tool for rapid development and deployment.



***********
Description
***********

ML-Flow-Client is a platform to simplefy machine learning development for insurance companies. 
It utilizes librariues like MlFlow https://mlflow.org/


***********
Quickstart
***********

Install using pip
=================
    
Install MLflow from PyPI via
----------------------------

.. code-block:: bash

    `ml-flow-client`


Create new project
==================
Go to the directory

.. code-block:: bash

    python -m mlflowclient.create_project



*************
Documentation
*************

Official documentation for Ml-Flow-Client can be found at https://ml-flow-py-client.readthedocs.io/en/latest/




Note
====

This project has been set up using PyScaffold 3.3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
