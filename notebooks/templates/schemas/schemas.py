from azure.core.exceptions import ResourceExistsError
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
import io
import inspect
import os

import mlflowclient


from src.mlflowclient.transfer.connector import upload_object

from schema import Schema, And, Use, Optional, SchemaError
from mlflowclient import upload_schema
# format old : new
var_schema_lk = {'Year' : 'calendar_year',
                 'Age Of Driver' : 'customer_age',
                 'Are Drivers Under23' : 'drivers_u_23',
                 'Bonus Class': 'bonus',
                 'Excess' : 'deductible', 
                 'Distance': 'mileage',
                 'Post Area': 'postal_code',
                 'Brand' : 'make', 
                 'Price': 'car_price',
                 'Total Weight' : 'total_weight',
                 'Year Of Model': 'model_year',
                 'Width' : 'width',
                 'Length' : 'length',
                 'Horse Power': 'horsepower',
                 'fuel': 'fuel',
                 'no_seats' : 'no_seats'
                 }

# for online schemas we can use, e.g.:
#  'calendar_year': lambda x: 2019 if x is None else Use(str),
# to set missing values to a default value. in offline schemas we wish to keep 
# missing values, because missing values are treated in the data preparation steps
type_schema = Schema([
                 {'calendar_year': lambda x: None if x is None else Use(str),
                  'customer_age':  lambda x: None if x is None else Use(int),
                  'drivers_u_23':  lambda x: None if x is None else Use(str),
                  'bonus':  lambda x: None if x is None else Use(str),
                  'deductible':  lambda x: None if x is None else Use(int),
                  'mileage':  lambda x: None if x is None else Use(str),
                  'postal_code':  lambda x: None if x is None else Use(str), # pad 0?
                  'make' : lambda x: None if x is None else Use(str),
                  'car_price' : lambda x: None if x is None else Use(int),
                  'total_weight' : lambda x: None if x is None else Use(int),
                  'model_year' : lambda x: None if x is None else Use(int),
                  'width' : lambda x: None if x is None else Use(int),
                  'length' : lambda x: None if x is None else Use(int),
                  'horsepower' : lambda x: None if x is None else Use(int),
                  'fuel' : lambda x: None if x is None else Use(str),
                  'no_seats' : lambda x: None if x is None else Use(int),
                  Optional('type'): And(str, Use(str.lower),
                                          lambda s: s in ('offline', 'online'))}
                                          ], 
                ignore_extra_keys = True
                )




upload_object(type_schema, object_name="fdafsd", object_category="schema", serialization_type="bytes/Pickle")




