from azure.storage.blob import (
    BlobServiceClient,
    BlobClient,
    ContainerClient,
    __version__,
)
from azure.core.exceptions import ResourceExistsError
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
import io
import inspect
import os
import dill as pickle
import _pickle as cPickle
import joblib
import json
import yaml
import sys
from mlflowclient.flow.schema import to_mlflow_schema
import mlflow.pyfunc
from mlflow.models.signature import ModelSignature
#from mlflow.types.schema import Schema, ColSpec
from schema import Schema


def get_connection_string():
    # path = os.getenv()
    if sys.platform == "linux":
        source = "/keybase/private/haadem/sources/azure.yaml"
    else:
        source = "K:/private/haadem/sources/azure.yaml"
    with open(source) as file:
        azure_access = yaml.load(file, Loader=yaml.FullLoader)

    connect_str = azure_access["Projects"]["analytikamlflow"][
        "storage_account"
    ]["connection_string"]
    # connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")

    return connect_str


def upload_schema(schema: Schema) -> str:
    #signature = ModelSignature(inputs=input_schema, outputs=output_schema)
    #schema = to_mlflow_schema(schema)
    table = "schema"
    connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")
    table_service = TableService(connection_string=connect_str)
    table_service.insert_entity(table, schema)
    return schema


def upload_model():
    class CustomModel(mlflow.pyfunc.PythonModel):

        def __init__(self, n):
            self.n = n

        def predict(self, context, model_input):
            return model_input.apply(lambda column: column + self.n)

    
    print(CustomModel)



def upload_object(
    upload_object: any,
    object_name: str,
    version: str = "v1.00.00",
    object_category: str = "postprocessor",
    serialization_type: str = "plain/text",
    standarization="ml-flow",
    object_type="function",
) -> None:

    source_code = b""
    container_name = object_category
    cloud_file_name = f"/{object_name}/{version}/"
    if serialization_type == "plain/text":
        if object_type == "function":
            source_code = inspect.getsource(upload_object)
            cloud_file_name = cloud_file_name + ".py"
        elif object_type == "plain/yaml":
            source_code = upload_object
            cloud_file_name = cloud_file_name + ".yaml"
        elif object_type == "dict":
            source_code = json.dumps(upload_object)
            cloud_file_name = cloud_file_name + ".yaml"
    elif serialization_type == "bytes/cPickle":
        source_code = cPickle.dumps(upload_object, 1)
        cloud_file_name = cloud_file_name + ".cpickle"
    elif serialization_type == "bytes/Pickle":
        source_code = pickle.dumps(upload_object, 1)
        cloud_file_name = cloud_file_name + ".pickle"
    elif serialization_type == "bytes/joblib":
        temp = io.BytesIO()
        joblib.dump(upload_object, temp)
        temp.seek(0)
        source_code = temp.read()
    else:
        print(f"{serialization_type} is not an option")
        return
    # Create the BlobServiceClient object which will be used to create a container client
    connect_str = get_connection_string()
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)
    # Create a blob client using the cloud_file_name as the name for the blob
    blob_client = blob_service_client.get_blob_client(
        container=container_name, blob=cloud_file_name
    )
    # Upload the created file
    try:
        blob_client.upload_blob(source_code)
    except ResourceExistsError:
        print(
            "The specified blob already exists. Select a different name with module_name='new_name'."
        )



def register_flow(id, name, type="prediction"):
    table = "flows"
    connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")
    table_service = TableService(connection_string=connect_str)
    flow = {
        "PartitionKey": "tasksSeattle",
        "RowKey": "001",
        "description": "Take out the trash",
        "priority": 200,
    }
    table_service.insert_entity(table, flow)


def register_processor(
    id,
    name,
    type="preprocessor",
    company="None",
    flow_id="1",
    data_model=None,
    standarization="ml-flow",
):
    table = "processors"
    connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")
    table_service = TableService(connection_string=connect_str)
    processor = {
        "PartitionKey": type,
        "RowKey": id,
        "description": "",
        "company": company,
        "flow_id": flow_id,
        "data_model": data_model,
        "standarization": standarization,
    }
    table_service.insert_entity(table, processor)


def register_schema(
    id,
    name,
    type="preprocessor",
    company="None",
    flow_id="1",
    data_model=None,
    standarization="ml-flow",
):
    table = "schema"
    connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")
    table_service = TableService(connection_string=connect_str)
    processor = {
        "PartitionKey": type,
        "RowKey": id,
        "description": "",
        "company": company,
        "flow_id": flow_id,
        "data_model": data_model,
        "standarization": standarization,
    }
    table_service.insert_entity(table, processor)


def register_model(
    id, type="prediction", flow_id=None, processor_id=None, data_model=None
):
    table = "models"
    connect_str = os.getenv("AZURE_STORAGE_CONNECTION_STRING")
    table_service = TableService(connection_string=connect_str)
    model = {
        "PartitionKey": "tasksSeattle",
        "RowKey": "001",
        "description": "Take out the trash",
        "priority": 200,
    }
    table_service.insert_entity(table, model)
