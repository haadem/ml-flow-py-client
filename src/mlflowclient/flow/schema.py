from typing import List

from mlflow.types import DataType, ColSpec, Schema
from mlflow.models.signature import ModelSignature
import inspect


def to_mlflow_schema(schema: dict) -> Schema:
    return Schema([ColSpec(value, key) for key, value in schema.items()])


def to_signature(input_schema: dict, output_schema: dict):
    input_schema = to_mlflow_schema(input_schema)
    output_schema = to_mlflow_schema(output_schema)
    signature = ModelSignature(inputs=input_schema, outputs=output_schema)
    return signature


def get_dtypes() -> List[str]:
    attributes = inspect.getmembers(
        DataType, lambda a: not (inspect.isroutine(a))
    )
    return [
        a[0]
        for a in attributes
        if not (a[0].startswith("__") and a[0].endswith("__"))
        and not (a[0] == "name")
        and not (a[0] == "value")
    ]
