def test():
    """[Summary]

    :param [ParamName]: [ParamDescription], defaults to [DefaultParamVal]
    :type [ParamName]: [ParamType](, optional)
    ...
    :raises [ErrorType]: [ErrorDescription]
    ...
    :return: [ReturnDescription]
    :rtype: [ReturnType]
    """
    pass


def test2(num: int) -> int:
    """[summary]

    Args:
        num (int): [description]

    Returns:
        int: [description]
    """
    pass
