import pandas as pd
from pydantic import BaseModel
import feather
import os
import copy

mtcars = pd.read_csv(
    "https://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
)


def preprocessing(df, is_json=False):
    if is_json:
        return "not implemented yet"
    df = copy.deepcopy(df)
    # check if needed variables are in data somehow?
    # if information like postal code is used, compare if sent postal
    #  code is in predefined list?

    df.loc[df.cyl > 6, "cyl"] = 6

    # df.loc[df['gear'] in [4, 3], 'gear_cat'] = '{4, 3}'
    # df.loc[df['gear'] == 5, 'gear_cat'] = '5'
    df["gear_cat"] = ["5" if x == 5 else "{4, 3}" for x in df["gear"]]

    return df


mtcars2 = preprocessing(mtcars)

path = "D:\\projects\\ml_flow\\preprocessing"
file_name = os.path.join(path, "mtcars.ftr")
mtcars2.to_feather(file_name)


class PreProcessData(BaseModel):

    cyl: int = 0
