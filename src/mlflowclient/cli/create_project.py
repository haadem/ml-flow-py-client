import pkg_resources
import shutil
import os

# print(pkg_resources.resource_exists('mlflowclient', 'templates/temp.ipynb'))

file_name = pkg_resources.resource_filename(
    "mlflowclient", "/cli/templates/temp.ipynb"
)

try:
    os.makedirs("notebooks")
except OSError as e:
    if e.errno != os.errno.EEXIST:
        raise
shutil.copy(file_name, os.getcwd() + "/notebooks/a.ipynb")
